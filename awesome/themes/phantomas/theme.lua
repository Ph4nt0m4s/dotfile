--[[
                                             
     Phantomas Awesome WM config 1.0         
                                             
                                             
--]]

theme                               = {}

theme_dir                           = os.getenv("HOME") .. "/.config/awesome/themes/phantomas"
theme.wallpaper                     = theme_dir .. "/background.png"

theme.font                          = "sans 8"

theme.fg_normal                     = "#aaaaaa"
theme.fg_focus                      = "#ffffff"
theme.fg_urgent                     = "#ffffff"
theme.fg_minimize                   = "#ffffff"

theme.bg_normal                     = "#222222"
theme.bg_focus                      = "#535d6c"
theme.bg_urgent                     = "#ff0000"
theme.bg_minimize                   = "#444444"
theme.bg_systray                    = theme.bg_normal

theme.border_width                  = "1"
theme.border_normal                 = "#3F3F3F"
theme.border_focus                  = "#7F7F7F"
theme.border_marked                 = "#91231c"

theme.titlebar_bg_normal            = "#ffffff"
theme.titlebar_bg_focus             = "#ffffff"

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty]
-- tasklist_[bg|fg]_[focus|urgent]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder[color|timeout|animate_timeout|radius|factor]
-- Example:
-- theme.taglist_bg_focus = "#ff0000"

theme.notify_fg                     = theme.fg_normal
theme.notify_bg                     = theme.bg_normal
theme.notify_border                 = theme.border_focus

theme.awful_widget_height           = 14
theme.awful_widget_margin_top       = 2

-- Variable set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon             = theme_dir .. "/icons/submenu.png"
theme.menu_height                   = "16"
theme.menu_width                    = "140"

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in you rc.lua
-- theme.bg_widget = "#cc0000"

-- Define the image to load
-- Load layout image
theme.layout_fairh                  = theme_dir .. "/layouts/fairh.png"
theme.layout_fairv                  = theme_dir .. "/layouts/fairv.png"
theme.layout_floating               = theme_dir .. "/layouts/floating.png"
theme.layout_magnifier              = theme_dir .. "/layouts/magnifier.png"
theme.layout_max                    = theme_dir .. "/layouts/max.png"
theme.layout_fullscreen             = theme_dir .. "/layouts/fullscreen.png"
theme.layout_tile                   = theme_dir .. "/layouts/tile.png"
theme.layout_tileleft               = theme_dir .. "/layouts/tileleft.png"
theme.layout_tilebottom             = theme_dir .. "/layouts/tilebottom.png"
theme.layout_tiletop                = theme_dir .. "/layouts/tiletop.png"
theme.layout_spiral                 = theme_dir .. "/layouts/spiral.png"
theme.layout_dwindle                = theme_dir .. "/layouts/dwindle.png"

-- Load arrl image
theme.arrl                          = theme_dir .. "/icons/arrl_sf.png"
theme.arrl_dl                       = theme_dir .. "/icons/arrl_dl_sf.png"
theme.arrl_ld                       = theme_dir .. "/icons/arrl_ld_sf.png"

-- Load widget image
theme.widget_ac                     = theme_dir .. "/icons/ac.png"
theme.widget_battery                = theme_dir .. "/icons/battery.png"
theme.widget_battery_low            = theme_dir .. "/icons/battery_low.png"
theme.widget_battery_empty          = theme_dir .. "/icons/battery_empty.png"
theme.widget_mem                    = theme_dir .. "/icons/mem.png"
theme.widget_cpu                    = theme_dir .. "/icons/cpu.png"
theme.widget_temp                   = theme_dir .. "/icons/temp.png"
theme.widget_net                    = theme_dir .. "/icons/net.png"
theme.widget_hdd                    = theme_dir .. "/icons/hdd.png"
theme.widget_music                  = theme_dir .. "/icons/note.png"
theme.widget_music_on               = theme_dir .. "/icons/note_on.png"
theme.widget_vol                    = theme_dir .. "/icons/vol.png"
theme.widget_vol_low                = theme_dir .. "/icons/vol_low.png"
theme.widget_vol_no                 = theme_dir .. "/icons/vol_no.png"
theme.widget_vol_mute               = theme_dir .. "/icons/vol_mute.png"
theme.widget_mail                   = theme_dir .. "/icons/mail.png"
theme.widget_mail_on                = theme_dir .. "/icons/mail_on.png"

theme.awesome_icon                  = theme_dir .. "/icons/unfa.png"

return theme
