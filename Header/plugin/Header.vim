""
""  EpiHeader.vim for EpiHeader in /home/cadet_g/.spf13-vim-3/.vim/bundle/EpiHeader/plugin
""
""  Made by Gabriel CADET
""  Login   <gabriel.cadet@epitech.eu>
""
""  Started on  Tue Mar 31 09:09:47 2015 Gabriel CADET
""  Last update lun. août 31 22:32:11 2015 Ph4nt0m4s
""

function! Plg_header_create(project)
    if &ft == ""
        return
    endif

    let s:pos           = line(".")
    let s:l_username    = substitute(system("git config --get user.name"), "\n", "", "")

    " Content Lines
    let s:l_file        = expand("%:t") . " for " . a:project . " in " . expand("%:p:h")
    let s:l_user        = "Made by " . s:l_username
    let s:l_email       = "Login   <" .
        \ substitute(system("git config --get user.email"), "\n", "", "") .
        \ ">"
    let s:l_date        = "Started on  " . strftime("%a %b %d %H:%M:%S %Y") . " " . s:l_username
    let s:l_cur_date    = "Last update " . strftime("%a %b %d %H:%M:%S %Y") . " " . s:l_username

    " Associative maps
    let ft_comment_map  = {
        \ "c":          [0, 0],
        \ "cpp":        [0, 0],
        \ "css":        [0, 0],
        \ "java":       [0, 0],
        \ "php":        [0, system("which php") . "<?php\n"],
        \ "make":       [1, 0],
        \ "text":       [1, 0],
        \ "sh":         [1, "/bin/bash\n"],
        \ "ruby":       [1, system("which ruby")],
        \ "perl":       [1, system("which perl")],
        \ "ocaml":      [2, system("which ocaml")],
        \ "python":     [1, system("which python")],
        \ "vim":        [3, 0],
        \ "tex":        [4, 0],
        \ "lisp":       [5, 0],
        \ "lex":        [6, 0],
        \ "pascal":     [7, 0],
        \ }
    let csce_comment_map = [
        \ ["/\*", "\*\*", "\*/"],
        \ ["##", "\##", "\##"],
        \ ["\(\*", "\*\*", "\*\)"],
        \ ["\"\"", "\"\" ", "\"\" "],
        \ ["%%", "\%%", "\%%"],
        \ [";;", ";;", ";;"],
        \ ["%{\n/\*", "\*\*", "\*/\n%}"],
        \ ["{", "\.. ", "}"],
        \ ]

    " Saving settings
    let s_fmt           = &fo
    let s_autoin        = &autoindent
    let s_smartin       = &smartindent
    let s_cin           = &cindent

    setl noautoindent nosmartindent nocindent
    setl fo-=c fo-=r fo-=o

    execute "normal! gg"
    if ft_comment_map[&ft][1] != "0"
        execute "normal! O#!" . ft_comment_map[&ft][1]
    endif
    execute "normal! i" .
        \ csce_comment_map[ft_comment_map[&ft][0]][0] . "\n" .
        \ csce_comment_map[ft_comment_map[&ft][0]][1] . " " .
        \ s:l_file . "\n" .
        \ csce_comment_map[ft_comment_map[&ft][0]][1] . "\n" .
        \ csce_comment_map[ft_comment_map[&ft][0]][1] . " " .
        \ s:l_user . "\n" .
        \ csce_comment_map[ft_comment_map[&ft][0]][1] . " " .
        \ s:l_email . "\n" .
        \ csce_comment_map[ft_comment_map[&ft][0]][1] . "\n" .
        \ csce_comment_map[ft_comment_map[&ft][0]][1] . " " .
        \ s:l_date . "\n" .
        \ csce_comment_map[ft_comment_map[&ft][0]][1] . " " .
        \ s:l_cur_date . "\n" .
        \ csce_comment_map[ft_comment_map[&ft][0]][2] . "\n\n"

    execute s:pos + 10
    unlet s:pos

    " Restituting settings
    let &fo=s_fmt
    unlet s_fmt
    let &autoindent=s_autoin
    unlet s_autoin
    let &smartindent=s_smartin
    unlet s_smartin
    let &cindent=s_cin
    unlet s_cin

    " Cleaning Content Lines
    unlet s:l_file
    unlet s:l_user
    unlet s:l_username
    unlet s:l_email
    unlet s:l_date
    unlet s:l_cur_date
    unlet ft_comment_map
    unlet csce_comment_map
endfunction

function! Plg_header_update()
    let s:l_username    = substitute(system("git config --get user.name"), "\n", "", "")
    
    execute "normal ma"
    silent! execute  "1," . 10 . "g/Last update .*/s/Last update .*/Last update " . strftime("%a %b %d %H:%M:%S %Y") . " " . s:l_username
    execute "normal `a"

    unlet s:l_username
endfunction

command! -nargs=1 Head call Plg_header_create(<f-args>)
autocmd Bufwritepre,filewritepre * call Plg_header_update()
